---
title: Home 🏠
description: Grey Software is a not-for-profit organization on a mission to democratize software education.
category: Info
position: 1
---

Grey Software is a not-for-profit organization on a mission to democratize software education.

We are doing this by:

- Creating open software that people can trust, love, and learn from!
- Creating an education ecosystem where learners worldwide can learn what professionals use!
- Creating opportunities for students to learn by collaborating remotely with open source developers!

## Our Objectives

Our 4 primary objectives are:

- 🎯 [Maintain software that people can trust, love, and learn from](https://gitlab.com/groups/grey-software/-/epics/18)
  - 🥅 [Make Focused Browsing the first success story in our app collection](https://gitlab.com/groups/grey-software/-/epics/22)
- 🎯 [Be a software education thought leader](https://gitlab.com/groups/grey-software/-/epics/2)
  - 🥅 [Build an inspiring and useful open software education ecosystem](https://gitlab.com/groups/grey-software/-/epics/6)
- 🎯 [Generate sustainable revenue through our software apps and education ecosystem](https://gitlab.com/groups/grey-software/-/epics/16)
  - 🥅 [Generate revenue through Github Sponsorships](https://gitlab.com/groups/grey-software/-/epics/3)
  - 🥅[Generate revenue by offering advertising space throughout our ecosystem](https://gitlab.com/groups/grey-software/-/epics/17)
- [Be a model organization for openness and transparency](https://gitlab.com/groups/grey-software/-/epics/1)
