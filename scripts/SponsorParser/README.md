
# Sponsor Data Parser

This script formats the Github Sponsors report into one that fits the sponsors.json file used by the landing website

## Script Execution

In the root scrpts/SponsorParser

 ```bash
node index.js '/path/to/read/file.json' 
```
